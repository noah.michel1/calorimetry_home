import os
import sys
import time
import json

from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


# This if statement enables you to write and run programs that test functions directly at the end of this file.
if __name__ == "__main__":
    import pathlib


    file_path = os.path.abspath(__file__)
    file_path = pathlib.Path(file_path)
    root = file_path.parent.parent
    sys.path.append(str(root))
    
from functions import m_json


def check_sensors() -> None:
    """Retrieves and prints the serial number and current temperature of all DS18B20 Temperature Sensors.

    This function utilizes the `w1thermsensor` library to interface with the DS18B20 temperature sensors.
    For each available sensor, it prints its serial number (or ID) and the current temperature reading.

    Examples:
    Assuming two DS18B20 sensors are connected:
    >>> check_sensors()
    Sensor 000005888445 has temperature 25.12
    Sensor 000005888446 has temperature 24.89

    """
    # TODO: Print serials and temperature values of all connected sensors.
    # HINT: Use the W1ThermSensor Library.
    # This line of code throws an exception. This is just to make sure you can see
    # all the code you need to refine. If you already know how to implement the program
    # or have done so, then you can safely delete the three lines of code below, as well
    # as this comment.
    
    #den code aus w1thermsensor kopieren und auslesen, dort dann die Serials in die ensprechenden json files speichern
    
    from w1thermsensor import W1ThermSensor
    
    for sensor in W1ThermSensor.get_available_sensors():
        print("Sensor %s has temperature %.2f" % (sensor.id, sensor.get_temperature()))

    # DONE #


def get_meas_data_calorimetry(metadata: dict) -> dict:
    """Collects and returns temperature measurement data from DS18B20 sensors based on the provided metadata.

    This function initializes sensor objects based on the metadata, prompts the user to start the
    measurement, and then continually reads temperature values until interrupted (e.g., via Ctrl-C).
    It logs the temperatures and the corresponding timestamps. Refer to README.md section
    "Runtime measurement data" for a detailed description of the output data structure.

    Args:
        metadata (dict): Contains details like sensor uuid, serials, and names.
                         Refer to README.md section "Runtime metadata" for a detailed description
                         of the input data structure.

    Returns:
        dict: A dictionary with sensor uuid as keys, and corresponding lists of temperatures and timestamps.

    Example:
        Input metadata:
        {
            "sensor": {
                "values": ["sensor_1", "sensor_2"],
                "serials": ["000005888445", "000005888446"],
                "names": ["FrontSensor", "BackSensor"]
            }
        }

        Output (example data after interruption):
        {
            "sensor_1": [[25.12, 25.15], [0, 2]],
            "sensor_2": [[24.89, 24.92], [0, 2]]
        }

    """
    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["values"]}
    sensor_list = [
        W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serials"]
    ]
    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                # TODO: Get experimental data.
                # This line of code throws an exception. This is just to make sure you can see
                # all the code you need to refine. If you already know how to implement the program
                # or have done so, then you can safely delete the three lines of code below, as well
                # as this comment.
                
                #temperatur und zeit in 2 variablen speichern
                
                temp = sensor.get_temperature() 
                now = time.time()
                print("%s: %.2f°C | % 3fs" % (metadata["sensor"]["values"][i], temp, now)) #jedes Mal uuid, temperatur und zeit auslesen
                
                #Messungen in Data speichern
                
                data[metadata["sensor"]["values"][i]][0].append(temp)
                data[metadata["sensor"]["values"][i]][1].append(now)
                
                time.sleep(0.5)
                # DONE #
            # Print an empty line for better readability in the console.
            print("")
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    """Logs the calorimetry measurement data into an H5 file.

    This function creates a folder (if not already present) and an H5 file with a
    specific structure. The data from the provided dictionaries are written to the
    H5 file, along with several attributes.

    Args:
        data (dict): Contains sensor data including temperature and timestamp.
                     Refer to README.md section"Runtime measurement data" for a detailed
                     description of the data structure.
        metadata (dict): Contains metadata. Refer to README.md section "Runtime metadata"
                         for a detailed description of the structure.
        data_folder (str): Path to the folder where the H5 file should be created.
        json_folder (str): Path to the folder containing the datasheets.

    """
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")

    # TODO: Add attribute to HDF5.
    # Set attributes for the H5 file based on the datasheets.
    
    #aktuelle Zeit:
    
    t = time.localtime()
    current_time = time.strftime("%H:%M:%S", t)
    
    #Group_json aufmachen und daten speichern
    
    with open(json_folder) as setup1:
        info = json.load(setup1)
        info1 = info["group"]
    
    #die ausgelesenen daten in attr speichern
    
    f.attrs["created"] = current_time
    f.attrs["experiment"] = info1["experiment"]
    f.attrs["group_number"] = info1["number"]
    f.attrs["authors"] = info1["author"]
    #for i in ["created", "experiment", "group_number", "authors"]:
        #if f.attrs[i] is None:
            # This line of code throws an exception. This is just to make sure you can see
            # all the code you need to refine. If you already know how to implement the program
            # or have done so, then you can safely delete the three lines of code below, as well
            # as this comment.  
    # DONE #

    # TODO: Write data to HDF5.
    # This line of code throws an exception. This is just to make sure you can see
    # all the code you need to refine. If you already know how to implement the program
    # or have done so, then you can safely delete the three lines of code below, as well
    # as this comment.
    
    #uuids auslesen
    
    list_uuids = []
    for i in data:
        list_uuids.append(i)
    
    #subgruppen erstellen mit uuid als name
    
    grp_uuid1 = grp_raw.create_group(list_uuids[0])
    grp_uuid2 = grp_raw.create_group(list_uuids[1])
    
    #temperaturen und zeiten in listen speichern (diese aus den dic data bekommen)
    
    list_temperature_sensor1 = data[list_uuids[0]][0]
    list_time_sensor1 = data[list_uuids[0]][1]
    
    #länge der seiteinheit kleiner machen da ich Probleme bei der Übertragung zu hdf5 oder bei jupyter nootebook hatte wo mir die Zahl immer abgerundet wurde
    count_list_time_sensor1 = len(list_time_sensor1)
    for i in range(0, count_list_time_sensor1):
        list_time_sensor1[i] = list_time_sensor1[i]-1700150000
    
    list_temperature_sensor2 = data[list_uuids[1]][0]
    list_time_sensor2 = data[list_uuids[1]][1]
    
    count_list_time_sensor2 = len(list_time_sensor2)
    for i in range(0, count_list_time_sensor2):
        list_time_sensor2[i] = list_time_sensor2[i]-1700150000
    
    #datasets erstellen mit jeweils den temperatur- und zeitlisten als werte
    
    dset_temperature1 = grp_uuid1.create_dataset("temperature_sensor1", data = list_temperature_sensor1,  maxshape = None, dtype = 'f')
    dset_time1 = grp_uuid1.create_dataset("time_sensor1", data = list_time_sensor1,  maxshape = None, dtype = 'f')
    
    dset_temperature2 = grp_uuid2.create_dataset("temperature_sensor2", data = list_temperature_sensor2,  maxshape = None, dtype = 'f')
    dset_time2 = grp_uuid2.create_dataset("time_sensor2", data = list_time_sensor2,  maxshape = None, dtype = 'f')
    
    #attribute für die subgruppen erstellen
    
    grp_uuid1.attrs["name_sensor1"] = metadata["sensor"]["names"][0]
    grp_uuid2.attrs["name_sensor2"] = metadata["sensor"]["names"][1]
    grp_uuid1.attrs["serial_sensor1"] = metadata["sensor"]["serials"][0]
    grp_uuid2.attrs["serial_sensor2"] = metadata["sensor"]["serials"][1]
    
    #attribute für den datenset time
    
    dset_time1.attrs["zeitkonvention"] = "Timestamp"
    dset_time2.attrs["zeitkonvention"] = "Timestamp"

    # DONE #

    # Close the H5 file.
    f.close()


if __name__ == "__main__":
    # Test and debug.
    pass

