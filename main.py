from functions import m_json
from functions import m_pck

# DIE ERSTE PART VON DEN CODE IST KOMMENTIERT DA ICH DIESEN FÜR DAS ZWEITE EXPERIMENT NICHT MEHR BRAUCHE

#path = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
#metadata = m_json.get_metadata_from_setup(path)
#m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets/", metadata)

#data = m_pck.get_meas_data_calorimetry(metadata)

#m_pck.logging_calorimetry(data, metadata, "/home/pi/calorimetry_home/data/daten", "/home/pi/calorimetry_home/datasheets/group.json")

#m_json.archiv_json("/home/pi/calorimetry_home/datasheets", "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json", "/home/pi/calorimetry_home/data/daten")

path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path)
m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets/", metadata)

data = m_pck.get_meas_data_calorimetry(metadata)

m_pck.logging_calorimetry(data, metadata, "/home/pi/calorimetry_home/data/daten1", "/home/pi/calorimetry_home/datasheets/group.json")

m_json.archiv_json("/home/pi/calorimetry_home/datasheets", "/home/pi/calorimetry_home/datasheets/setup_newton.json", "/home/pi/calorimetry_home/data/daten1")
